import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertService } from '@app/core/shared/services/alert.service';
import { AuthService } from '@app/core/shared/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  model: any = {};
  loading = false;
  returnUrl: string;

  constructor( private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthService,
    private alertService: AlertService) { }

  ngOnInit() {
    this.authenticationService.logout();
    //this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
    this.returnUrl ='/dashboard';
  }

  login() {
    this.loading = true;
    //alert('login');
   //event.preventDefault();
    this.authenticationService.login(this.model.username, this.model.password)
        .subscribe(
            data => {
              localStorage.setItem('currentUser',JSON.stringify(data));
              //console.log("respone!");
              //console.log(data);               
                this.alertService.success;
                this.router.navigate(['dashboard']);//([this.returnUrl]);
            },
            error => {
                this.alertService.error(error._body);
                this.loading = false;
            });
  }
}
