import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {  MatButtonModule, MatToolbarModule, MatMenuModule, MatCardModule, MatIconModule, MatAutocompleteModule, MatInputModule, MatFormFieldModule, MatStepperModule } from '@angular/material';

@NgModule({
  imports: [MatButtonModule, 
    MatToolbarModule,
    MatMenuModule,
    MatCardModule,
    MatIconModule,
    MatFormFieldModule,
    MatInputModule,
    MatAutocompleteModule,
    MatStepperModule],
  exports: [MatButtonModule, 
    MatToolbarModule,
    MatMenuModule,
    MatCardModule,
    MatIconModule,
    MatFormFieldModule,
    MatInputModule,
    MatAutocompleteModule,
    MatStepperModule],
  declarations: []
  })

export class MaterialAppModule { }