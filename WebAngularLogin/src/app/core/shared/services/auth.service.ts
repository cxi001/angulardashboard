import { Injectable } from "@angular/core";
//import { HttpClient } from "selenium-webdriver/http";
import { HttpHeaders, HttpClient,HttpClientModule } from "@angular/common/http";
//import { Http } from "@angular/http";
import { AppConfig } from "@app/app.config";
import 'rxjs/add/operator/map';
import { AlertService } from "@app/core/shared/services/alert.service";

@Injectable()
export class AuthService{    
    
    constructor(private http:HttpClient, private config: AppConfig){}

    login(username:string ,password:string){
        //console.log("start login!");
      //  let usr = localStorage.getItem('currentUser')
        //console.log('user before:'+usr);
        return this.http.post(this.config.apiUrl+'/Users/authenticate', { UserEmail: username, password: password })
                // .map((response: Response) => {  
                //     console.log(response.json());
                //     let user = response.json();                                   
                //     if(user ){                        
                //     // store user details and jwt token in local storage to keep user logged in between page refreshes
                //     localStorage.setItem('currentUser', JSON.stringify(user));                
                //     }                    
                // } 
                .map((response:Response)=>{   
                    let user = response;
                    console.log('user json:'+JSON.stringify(user));
                    console.log('Token json:'+user['token']);
                    if(user && user['token'])   
                    {
                        localStorage.setItem('currentUser', JSON.stringify(response));   
                        console.log('user now:'+localStorage.getItem('currentUser'));
                    }              
                    
                }
            );      
    }

    logout() {        
        localStorage.removeItem('currentUser');
    }
        
}