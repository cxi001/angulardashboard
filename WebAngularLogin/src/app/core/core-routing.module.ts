import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from '@app/core/login/login.component';
import { RegisterComponent } from '@app/core/register/register.component';
import { DashboardComponent } from '@app/core/dashboard/dashboard.component';
import { AuthGuardGuard } from '@app/shared/guard/auth-guard.guard';
 

const coreRoutes: Routes=[
    {        
        path: '',
        redirectTo: 'login',
        pathMatch:'full'
    },
    {        
        path: 'login',
        component:LoginComponent
    },
    {        
        path: 'register',
        component:RegisterComponent
    },
    {
        path:'dashboard',
        component:DashboardComponent
        ,canActivate:[AuthGuardGuard] 
    },
    
    { path: '**',  redirectTo: 'login'}   
];

@NgModule({
    imports: [RouterModule.forRoot(coreRoutes)],
    exports: [RouterModule]
})
export class CoreRoutingModule{}