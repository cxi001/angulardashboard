import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { MaterialAppModule } from  './shared/material.module';
import { CoreRoutingModule } from './core-routing.module';
import { CoreComponent } from './core.component';
import { AuthGuardGuard } from  '@app/shared/guard/auth-guard.guard';
import { BrowserModule } from '@angular/platform-browser';
import { DashboardComponent } from '@app/core/dashboard/dashboard.component';
import { AlertComponent } from '@app/core/shared/alert/alert.component';
import { AlertService } from '@app/core/shared/services/alert.service';
import { AuthService } from '@app/core/shared/services/auth.service';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';


@NgModule({
  imports: [
    MaterialAppModule,
    CoreRoutingModule, 
    BrowserModule,      
    FormsModule, 
    HttpClientModule,    
    CommonModule
  ],
  exports:[
    MaterialAppModule,
    LoginComponent, 
    RegisterComponent,
    CoreRoutingModule,
    CoreComponent,    
    HttpClientModule,
    AlertComponent],
  declarations: [
    LoginComponent, 
    RegisterComponent, 
    DashboardComponent, 
    CoreComponent,    
    AlertComponent],
  providers:[AuthGuardGuard,AlertService,AuthService]
  
})
export class CoreModule { }
