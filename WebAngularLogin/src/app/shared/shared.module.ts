import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialAppModule } from '../core/shared/material.module';
import { HeaderComponent } from './header/header.component';


@NgModule({
  imports: [
    CommonModule,    
    MaterialAppModule
  ],  
  exports: [HeaderComponent],
  declarations: [HeaderComponent] 
})
export class SharedModule { }
