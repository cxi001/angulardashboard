import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {  MatButtonModule, MatToolbarModule, MatMenuModule, MatCardModule, MatIconModule, MatAutocompleteModule, MatInputModule, MatFormFieldModule } from '@angular/material';

@NgModule({
    imports: [MatButtonModule, 
      MatToolbarModule,
      MatMenuModule,
      MatCardModule,
      MatIconModule,
      MatFormFieldModule,
      MatInputModule,
      MatAutocompleteModule],
    exports: [MatButtonModule, 
      MatToolbarModule,
      MatMenuModule,
      MatCardModule,
      MatIconModule,
      MatFormFieldModule,
      MatInputModule,
      MatAutocompleteModule],
    declarations: []
  })

export class MaterialAppModule { }